console.log("I'm starting");

// input the word and show result on the right V2
const input = document.querySelector('input');
document.getElementById("text-input").addEventListener('input', debounce(processInput,1000));

const suggestedReplacementKeywords = document.getElementById("suggested-replacement-keywords");
const suggestedReplacementSentence = document.getElementById("suggested-replacement-sentence");

function processInput(e) {
  suggestedReplacementKeywords.innerHTML = 'good things take time, wait for a few seconds...';
  url = '/api/v1/spell-checker.json?query=' + encodeURIComponent(e.target.value);
  ajaxCall(url, printOutputResults);
};

function printOutputResults(e) {
  suggestedReplacementKeywords.innerHTML = ''
  e["suggested_replacement_keywords"].forEach(keyword => {
    suggestedReplacementKeywords.innerHTML += "<li>" + keyword["word"] + " could be replaced by " + keyword["suggested_words"].join(", ") + "</li>";
  });
  if (e["suggested_replacement_keywords"][0]["suggested_words"].length == 0) { suggestedReplacementKeywords.innerHTML = "No replacement suggestions found..."}
  suggestedReplacementSentence.innerHTML = e["corrected_sentence"];
  if (e["corrected_sentence"] == "") { suggestedReplacementSentence.innerHTML = "No replacement suggestions found..."}
};

function ajaxCall(url, callbackFunction) {
  var xmlhttp = new XMLHttpRequest();
  console.log("calling: " + url);

  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
      if (xmlhttp.status == 200) {
        callbackFunction(JSON.parse(xmlhttp.responseText));
      }
      else if (xmlhttp.status == 400) {
        alert(url + 'returns an error 400');
      }
      else {
         alert(url + 'returns an unknown error');
      }
    }
  };

  xmlhttp.open("GET", url, true);
  xmlhttp.send();
};

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};