require_relative '../spell_checker.rb'

RSpec.describe 'Spell checker' do
  context '(Unit test) when computing distance between keywords' do
    checker = SpellChecker.new
    it 'return a distance between 2 keywords, by counting how many letters have been removed' do
      expect(checker.compute_distance_between_keywords(keyword1: "cntact", keyword2: "contact")).to eq(1)
      expect(checker.compute_distance_between_keywords(keyword1: "baker", keyword2: "breaker")).to eq(2)
    end

    it 'return a distance between 2 keywords, by counting how many letters have been changed' do
      expect(checker.compute_distance_between_keywords(keyword1: "photoelectric", keyword2: "photoolectoio")).to eq(3)
    end

    it 'return a distance between 2 keywords, by counting how many letters have been added' do
      expect(checker.compute_distance_between_keywords(keyword1: "photoelectric", keyword2: "photoelectrics")).to eq(1)
    end  

    it 'return a distance between 2 keywords, by counting how many signs have been added' do
      expect(checker.compute_distance_between_keywords(keyword1: "photoelectric", keyword2: "photo-electric")).to eq(1)
    end

    it "returns the same distance when comparing 2 keywords, regardless of the order of comparison" do
      expect(checker.compute_distance_between_keywords(keyword1: "breaker", keyword2: "baker")).to eq(2)
      expect(checker.compute_distance_between_keywords(keyword1: "baker", keyword2: "breaker")).to eq(2)
    end
  end

  context '(Unit test) when returning corrected keywords' do
    checker = SpellChecker.new
    it 'only return keyword if this keyword is exactly found in dictionary' do
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "contactor", 
          dictionary: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["contactor"])
    end

    it 'always returns the suggested keywords in decreasing order of count in dictionary' do
      keywords = checker.return_N_keywords_with_lowest_distance_from_input_keyword(
        keyword_to_analyze: "contct", 
        dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        quantity: 2
        )
      dictionary = CSV.read('./spec/data_for_tests/kyklo_test_dictionary.csv')
      counts = dictionary.select {|row| keywords.include?(row[0])}.map { |selected_row| selected_row[1].to_i }
      expect(counts.sort_by { |e| -e }).to eq(counts)
    end

    it 'returns multiple keywords from dictionary with the lowest distance from input keyword and sorted by decreasing count of presence in KYKLO dictionary' do
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "contct", 
          dictionary_csv: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["contact"])
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "breakr", 
          dictionary_csv: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["breaker", "break", "breaks"])
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "sensro", 
          dictionary_csv: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["sensor", "sensr", "sensrs"])
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "cilinder", 
          dictionary_csv: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["cylinder"])
      expect(
        checker.return_N_keywords_with_lowest_distance_from_input_keyword(
          keyword_to_analyze: "contractor", 
          dictionary_csv: "./data/kyklo_dictionary.csv",
          quantity: 2
          )
        ).to eq(["contactor"])
    end

    it 'checks mispelt keywords from a sentence and return results which contains a feedback and suggested keywords when the input words are incorrect' do
      expect(
        checker.return_suggested_keywords(
          keyword_to_analyze: "Magnatic cotactor - Direct On Line (DOL) / FVNR (Full-Voltage Non-Rv.) - Mitsubishi Electric - Screw-clamp connections - DIN rail / Surface mounting", 
          dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv"
          ).map {|e| e[:suggested_words]}.flatten
        ).to include("magnetic", "contactor")
      expect(
        checker.return_suggested_keywords(
          keyword_to_analyze: "Contractor", 
          dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv"
          ).map {|e| e[:suggested_words]}.flatten
        ).to eq(["contactor"])
    end

    it 'checks mispelt keywords from a sentence (not taking into account strings containing numbers and signs)' do
      expect(
        checker.return_suggested_keywords(
          keyword_to_analyze: "Magnetic contctor - Schneider Electric (TeSys D) - DOL (Direct On Line) - 3 poles (3P) - Rated power (380-415Vac; AC-3) 4kW - Rated current (440vac; AC-3) 9A - Control coil voltage 24Vdc - 1NO+1NC auxilliary contacts - Screw-clamp terminals",
          dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv"
          )
        ).to include(*[
        {
          word: "contctor",
          word_quality: "bad",
          suggested_words: [
            "contactor"
          ]
        },      
        {
          word: "auxilliary",
          word_quality: "bad",
          suggested_words: [
            "auxiliary"
          ]
        }
      ])
      expect(checker.return_suggested_keywords(
        keyword_to_analyze: "Inductive proximity sensor / swicth - Turck (NI25 Q20) - product ID 1602700 - 25mm sensing range - 1 x digital output (10-30Vdc; PNP) - Pre-wired with 2m / 6.5ft cabel terminated with bare end flying leads - Revtangular body - Rated for -25...+70°C ambient - Supply voltage 10-30Vdc (12Vdc / 24Vdc nom.) - IP67 - Material PBT (huosing) + PBT (sensing face) + PBT (front cap) + TPU (cable) - Maximum switching frequency 250Hz",
        dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv"
        )
      ).to include(*[
        {
          word: "swicth",
          word_quality: "bad",
          suggested_words: [ "switch" ]
        },      
        {
          word: "cabel",
          word_quality: "bad",
          suggested_words: [ "cable", "label", "kabel" ]
        },      
        {
          word: "revtangular",
          word_quality: "bad",
          suggested_words: [ "rectangular" ]
        },
        {
          word: "huosing",
          word_quality: "bad",
          suggested_words: [ "housing" ]
        }
      ])
      expect(
        checker.return_suggested_keywords(
        keyword_to_analyze: "Electric linear actuater with coupler for 5mm mtr. shaft + piston magnet + sw. track (position 1) - Bimba (Original line Eletcric Actuator series) - 1-1/2\" diam. body (75lb thrust) - 1/8\" spindle pitch / lead - 1\" stroke (S1\") - Tapped holes mounting / Standard mtr. mount - 7/16-20 UNF (male) threaded rod - Stainless steel 304 rod / Aluminium 6061-T6511 piston / Steel 52100 angular bearing",
        dictionary_csv: "./spec/data_for_tests/kyklo_test_dictionary.csv"
        )
      ).to include(*[
        {
          word: "actuater",
          word_quality: "bad",
          suggested_words: [ "actuator", "actuated", "actuate" ]
        },      
        {
          word: "eletcric",
          word_quality: "bad",
          suggested_words: [ "electric" ]
        }
      ]) 
    end
  end
  context '(Unit test) when receiving a full sentence for spell correction' do
    checker = SpellChecker.new
    it 'returns the sentence with unchanged keywords (including case) when those are already in KYKLO dictionary' do
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "Breaker tHree Poles"
        )
      ).to eq("Breaker tHree Poles")
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "Contactor FVNR"
        )
      ).to eq("Contactor FVNR")
    end
    it 'returns the sentence with unchanged keywords when those contain numbers' do
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "LC1D09M7 100A"
        )
      ).to eq("LC1D09M7 100A")
    end
    it 'returns SKUs codes unmodified' do
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "LC1D09M7"
        )
      ).to eq("LC1D09M7")
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "MSF4800A-30-0600-30-0600-30-0600-30-0600-10X-10R-030030030XI-030030030RI-RM630"
        )
      ).to eq("MSF4800A-30-0600-30-0600-30-0600-30-0600-10X-10R-030030030XI-030030030RI-RM630")
    end
    it 'returns corrected keywords when a close match was found in KYKLO dictionary' do
      expect(checker.return_corrected_sentence(
        dictionary: "./spec/data_for_tests/kyklo_test_dictionary.csv",
        sentence: "Braker 100A thee pole"
        )
      ).to eq("breaker 100A three pole")
    end
  end
  context '(End to end test) when receiving a full sentence for spell correction' do
    checker = SpellChecker.new
    it 'returns expected sentences for common types of searches' do
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "contator 9 amps"
        )
      ).to eq("contactor 9 amps")
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "duoble acting actuater 10mm boer"
        )
      ).to eq("double acting actuator 10mm bore")
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "liner actuater cantlever"
        )
      ).to eq("linear actuator cantilever")
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "linera actuater cantlever"
        )
      ).to eq("linear actuator cantilever")
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "LC1D09M7 contcator 230 wolts ac"
        )
      ).to eq("LC1D09M7 contactor 230 volts ac")
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "breker for 480 vlts dc"
        )
      ).to eq("breaker for 480 volts dc")
    end
    it 'returns words of 3 or less characters untouched in a sentence' do
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: "ACB 3200 amps"
        )
      ).to eq("ACB 3200 amps")
      string_length = 3
      sentence_to_test = []
      10.times do |variable|
        sentence_to_test << rand(36**string_length).to_s(36)
      end
      sentence_to_test = sentence_to_test.join(' ')
      expect(checker.return_corrected_sentence(
        dictionary: "./data/kyklo_dictionary.csv",
        sentence: sentence_to_test
        )
      ).to eq(sentence_to_test)
    end  
    it 'returns correct hyphenated keywords from dictionary untouched in a sentence' do
      keywords_with_hyphen = ['photo-electric', 'one-touch']
      keywords_with_hyphen.each do |keyword_with_hyphen|
        expect(checker.return_corrected_sentence(
          dictionary: "./data/kyklo_dictionary.csv",
          sentence: keyword_with_hyphen
          )
        ).to eq(keyword_with_hyphen)
      end
    end  
  end
end