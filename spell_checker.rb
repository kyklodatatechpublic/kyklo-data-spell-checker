require "csv"
require "damerau-levenshtein"
require "json"

class SpellChecker
	def initialize
		# @dictionary_words ||= load_dictionary(location: 'lexicon_combined.json').keys.to_set
		@dictionary_words ||= [].to_set
		@dictionary_words_with_count = {}
	end
	
	def compute_distance_between_keywords(keyword1:, keyword2:)
		dl ||= DamerauLevenshtein
		return dl.distance(keyword1, keyword2)
	end

	def return_N_keywords_with_lowest_distance_from_input_keyword(keyword_to_analyze:, dictionary: nil, dictionary_csv: nil, quantity:)
		unless dictionary.nil?
			@dictionary_words = load_dictionary(location: dictionary).to_set
			@dictionary_words_with_count = load_dictionary_with_count(location: dictionary)
		end

    results = {}

    if @dictionary_words.include?(keyword_to_analyze) then
      return [keyword_to_analyze]
    else
      @dictionary_words.each.with_index do |dictionary_keyword, idx|
      	results[dictionary_keyword] = compute_distance_between_keywords(keyword1: keyword_to_analyze, keyword2: dictionary_keyword)
      end
    end
    output = results.map{|keyword, distance| [keyword, distance]}
    .select { |e| e[1] == 1 }
    .map { |e| e[0] }
    .sort_by { |keyword | -@dictionary_words_with_count[keyword] }
    # .map { |keyword| [keyword, @dictionary_words_with_count[keyword]] }

    # output.each {|keyword| puts "#{keyword} => #{@dictionary_words_with_count[keyword]}"}

    return output
	end

	def load_dictionary(location:)
		dictionary = []
		File.open(location, 'r:bom|utf-8') do |f|
			dictionary = CSV.parse(f.read, headers: true).map(&:to_h).select { |row| row["valid"] == "TRUE" }.map { |row| row['word'] }
		end
		return dictionary
 	end

	def load_dictionary_with_count(location:)
		dictionary = {}
		File.open(location, 'r:bom|utf-8') do |f|
			CSV.parse(f.read, headers: true).map(&:to_h).select { |row| row["valid"] == "TRUE" }.each do |row|
				dictionary[row['word']] = row['count'].to_i
			end
		end
		return dictionary
	end

	def return_suggested_keywords(keyword_to_analyze:, dictionary: nil, dictionary_csv: nil)
		# @dictionary_words ||= load_dictionary(location: dictionary).keys.to_set

		results = []
		array_of_keywords = keyword_to_analyze.downcase.gsub(/[\[\]\(\)]/,"").split(/[\s]/).uniq
		array_of_keywords.each do |word|
			if !@dictionary_words.include?(word) && (word.match(/[^A-Za-z\-\.]/).nil?) && (word.length > 2)
				results << { 
		          word: word,
		          word_quality: "bad",
		          suggested_words: return_N_keywords_with_lowest_distance_from_input_keyword(
		          	keyword_to_analyze: word, 
		          	dictionary: dictionary, 
		          	dictionary_csv: dictionary_csv, 
		          	quantity: 2)
		        }
			end
		end
		return results
	end

	def return_corrected_sentence(dictionary:, sentence:)
		# @dictionary_words = CSV.read(dictionary).map { |row| row[0] } unless dictionary.nil?
		@dictionary_words = load_dictionary(location: dictionary).to_set
		correct_sentence = []
		sentence.split(" ").each do |token|
			# p token
			if token.length < 4 then
				correct_sentence << token	
			elsif @dictionary_words.include?(token.downcase) || token.match(/[0-9]/).to_a.length > 0 then
				# p token + " is in dictionary"
				correct_sentence << token	
			else
				# p "token #{token} is not in dictionary!"
				correct_sentence << return_suggested_keywords(
				  keyword_to_analyze: token, 
				  dictionary: dictionary
				  ).map {|e| e[:suggested_words]}.flatten.first
			end
		end
		return correct_sentence.join(" ")
	end
end