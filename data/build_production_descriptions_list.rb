require 'csv'
require 'pg'
require 'zlib'

class DbConnection
  def initialize(connection_string:)
    @credentials = connection_string
  end

  def get_records_from_db(statement:)
    begin
      connect
      @connection.send_query(statement)
      return @connection.get_result
    rescue PG::Error => e
      puts e.message
    ensure
      @connection&.close if @connection
    end
  end

  private

  def connect
    @connection ||= PG::Connection.open(@credentials)
  end
end

puts "Please input the DB connection string (obtain it in Heroku admin):"
connection_string = gets.chomp
system "cls"

puts "Connecting to products DB..."
start = Time.now

connection1 = DbConnection.new(connection_string: connection_string)
products_count = connection1.get_records_from_db(statement: "SELECT COUNT(id) FROM products;")[0]["count"].to_i 
# puts "#{products_count} products' main functions to download"

batch_size = 50000 # progressively increase to 50k

remaining_products_to_download = products_count
offset = 0




csv_data = CSV.generate(headers: true) do |csv|
  while remaining_products_to_download >= 0 do
    puts "#{remaining_products_to_download} products remaining to download. downloading batch #{offset/batch_size+1}/#{products_count/batch_size+1} (#{batch_size} SKUs)"
    connection2 = DbConnection.new(connection_string: connection_string)
    
    main_functions_in_production_sql_statement = "
      SELECT 
        description_translations->'en' AS description_en,
        updated_at AS description_last_updated_at
      FROM products
      WHERE 
        products.id BETWEEN #{offset} AND #{offset + batch_size - 1}
        ;
      "

    results = connection2.get_records_from_db(statement: main_functions_in_production_sql_statement)
    results.each do |row|
      csv << row.values
    end
    offset += batch_size
    remaining_products_to_download -= batch_size
  end
end

original_file_name = 'descriptions_published_in_production.csv'
zip_file_name = 'descriptions_published_in_production.gz'
Zlib::GzipWriter.open(zip_file_name) do |gz|
  gz.mtime = Time.now
  gz.orig_name = original_file_name
  gz.write csv_data
end

puts "Done! main functions data prepared in #{(Time.now - start).round(1)}s"