# Main objective: merge all this into search-magic/converter (after a 1st pass with converter)
# Internal use:
# 	Keep only meaningful semantic keywords in dictionary
# 	Only datasheet and filter field to the exception of [EAN code, UPC code, Description]

# External use:
# 	only keep semantic keywords ?

require 'csv'
require 'set'
require 'zlib'

dictionary = {}

lexicon1 ||= CSV.read('../lexicon-general-en.csv') 
lexicon2 ||= CSV.read('../lexicon-specific.csv') 
lexicon ||= (lexicon1|lexicon2).flatten.to_set
puts "opening .gz file"
csv_string = ""
Zlib::GzipReader.open("descriptions_published_in_production.gz") {|gz|
	csv_string << gz.read
}

puts '  Parsing CSV data (this will take around 30s)...' if $verbose

CSV.parse(csv_string).each.with_index do |row, idx|
	next if row[0].nil?
	# next if idx > 50000
	last_updated_date = DateTime.parse(row[1])
	keywords_in_description = row[0].gsub(/[\[\]\(\)]/,"").split(/[\s]/).select {|e| e != "" && !e.match(/[^A-Za-z\-]/) && e.length > 2}
	keywords_in_description.each do |keyword|
		keyword = keyword.downcase
		if dictionary[keyword].nil? then
			semantic_or_not = lexicon.include?(keyword.downcase) ? true : false 
			dictionary[keyword] = {
				keyword: keyword.downcase,
				count: 1,
				semantic: semantic_or_not,
				last_updated_date: last_updated_date
			}
		else
			dictionary[keyword][:count] += 1
		end
		if !dictionary[keyword].nil? 
			if last_updated_date > dictionary[keyword][:last_updated_date]
				dictionary[keyword][last_updated_date] = last_updated_date
			end
		end
	end
	puts "processed line #{idx}" if idx % 10000 == 0
end

CSV.open('./kyklo_keywords_from_production.csv', 'wb', headers: true) do |csv|
	csv << [:word, :count, :semantic, :last_updated_date]
	dictionary.each do |keyword, data|
		csv << {
			word: keyword,
			count: data[:count],
			semantic: data[:semantic],
			last_updated_date: data[:last_updated_date]
		}
	end
end

