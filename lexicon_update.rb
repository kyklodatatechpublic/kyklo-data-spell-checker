# File to phase out. Superseded by build_lexicon.rb


require "csv"
require "damerau-levenshtein"
require "json"

lexicon1 = CSV.read('lexicon-general-en.csv')
lexicon2 = CSV.read('lexicon-specific.csv')
lexicon ||= (lexicon1|lexicon2).flatten
rows = Hash.new
lexicon.each do |row|
  rows[row] = 1
end
File.write('./lexicon_combined.json', JSON.dump(rows))