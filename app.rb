require 'sinatra'
require 'slim'
require_relative 'spell_checker.rb'

get '/' do
	slim :index
end

before '/api/v1/spell-checker.json' do
  headers "Content-Type" => "application/json; charset=utf-8"
  headers "Access-Control-Allow-Origin" => "*"
end

get '/api/v1/spell-checker.json' do
	@original_query = params[:query] || ""

	@checker ||= SpellChecker.new
	return {
		initial_query: @original_query,
		suggested_replacement_keywords: @checker.return_suggested_keywords(
			keyword_to_analyze: @original_query, 
			dictionary: "./data/kyklo_dictionary.csv"
			),
		corrected_sentence: @checker.return_corrected_sentence(
			dictionary: "./data/kyklo_dictionary.csv",
			sentence: CGI.unescape(@original_query
				)
			)
	}.to_json
end